angular.module('simpleSocial.suggestionServices', [])

/**
 * A simple example service that returns some data.
 */
    .factory('SuggestionService', function() {
        // Might use a resource here that returns a JSON array

        // Some fake testing data
        var suggestions = [
            { id: 0, name: 'Meet with John', type: 'Friend Birthday', description: 'Your friend John is having a birthday', date: '14/10/2014', time: '21:30', meetImage:'./img/avatars/1.jpg', suggestionType: 'meeting up for drinks',  venue: 'Bank Hotel, Newtown',
                attendees:[
                    {id:0, name:'Johnny Thomas', picture:'./img/avatars/1.jpg'}
                ]},
            { id: 1, name: 'Hang out with Monica', type: 'Friend Neglect', description: 'It\'s been 90 days since your last recorded meet with Monica', date: '20/10/2014', time: '21:30', meetImage:'./img/avatars/2.jpg', suggestionType: 'catching up over coffee', venue: 'Cafe Alma, Darlinghurst',
                attendees:[
                    {id:0, name:'Monica Leung', picture:'./img/avatars/2.jpg'}
                ]},
            { id: 2, name: 'Catch up with Mark', type: 'Friend Available', description: 'Mark is back in Sydney this week', date: '01/10/2014', time: '21:30', meetImage:'./img/avatars/3.jpg', suggestionType: 'organising a group dinner',   venue: 'BBQ King, Haymarket',
                attendees:[
                    {id:0, name:'Mark Pearson', picture:'./img/avatars/3.jpg'},
                    {id:1, name:'Carla Neeson', picture:'./img/avatars/4.jpg'},
                    {id:2, name:'Jake Godwin', picture:'./img/avatars/7.jpg'},
                    {id:3, name:'Katie Moorebank', picture:'./img/avatars/8.jpg'}

                ]}

        ];

        return {
            all: function() {
                return suggestions;
            },
            get: function(suggestionId) {
                // Simple index lookup
                return suggestions[suggestionId];
            }
        }
    });