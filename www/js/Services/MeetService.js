angular.module('simpleSocial.meetServices', [])

/**
 * A simple example service that returns some data.
 */
    .factory('MeetService', function() {
        // Might use a resource here that returns a JSON array

        // Some fake testing data
        var meets = [
            { id: 0, name: 'John\'s Birthday',  date: '01/07/2014', time: '21:30', venue:'Fifty-Fifty Bar, Darlinghurst', meetImage:'./img/meets/icon-cake.png', host:'John Doe', accepted:'True',declined:'False',     isNew:'False',
                attendees:[
                    {id:0, name:'Johnny Thomas', attendancestatus: 'Attending', profilePicture:'./img/avatars/1.jpg'},
                    {id:1, name:'Gordon Lightfoot', attendancestatus: 'Awaiting Response', profilePicture:'./img/avatars/3.jpg'},
                    {id:2, name:'Mark Pearson', attendancestatus: 'Not Attending', profilePicture:'./img/avatars/4.jpg'},
                    {id:3, name:'Monica Leung', attendancestatus: 'Attending', profilePicture:'./img/avatars/2.jpg'},
                    {id:4, name:'Toby Ray', attendancestatus: 'Awaiting Response', profilePicture:'./img/avatars/7.jpg'},
                    {id:5, name:'Phoebe Nelson', attendancestatus: 'Attending', profilePicture:'./img/avatars/6.jpg'},
                    {id:6, name:'Mike Carr', attendancestatus: 'Awaiting Response', profilePicture:'./img/avatars/5.jpg'},
                    {id:7, name:'Colin Ray', attendancestatus: 'Attending', profilePicture:'./img/avatars/8.jpg'}
                ]},
            { id: 1, name: 'Graduation Party',  date: '20/07/2014', time: '20:30', venue:'Lord Nelson, The Rocks', meetImage:'./img/meets/icon-beer.png', host:'Catie Neil', accepted:'True',declined:'False', isNew:'False',
                attendees:[
                    {id:0, name:'Johnny Thomas', attendancestatus: 'Attending', profilePicture:'./img/avatars/1.jpg'},
                    {id:1, name:'Gordon Lightfoot', attendancestatus: 'Awaiting Response', profilePicture:'./img/avatars/3.jpg'},
                    {id:2, name:'Mark Pearson', attendancestatus: 'Not Attending', profilePicture:'./img/avatars/4.jpg'},
                    {id:3, name:'Monica Leung', attendancestatus: 'Attending', profilePicture:'./img/avatars/2.jpg'},
                    {id:4, name:'Toby Ray', attendancestatus: 'Awaiting Response', profilePicture:'./img/avatars/7.jpg'},
                    {id:5, name:'Phoebe Nelson', attendancestatus: 'Attending', profilePicture:'./img/avatars/6.jpg'},
                    {id:6, name:'Mike Carr', attendancestatus: 'Awaiting Response', profilePicture:'./img/avatars/5.jpg'},
                    {id:7, name:'Colin Ray', attendancestatus: 'Attending', profilePicture:'./img/avatars/8.jpg'}
                ]},
            { id: 2, name: 'Random Dinner party',  date: '22/08/2014', time: '17:30', venue:'Bar Reggio, Darlinghurst', meetImage:'./img/meets/icon-meal.png', host:'Mark Greyson', accepted:'False', isNew:'False',
                attendees:[
                    {id:0, name:'Johnny Thomas', attendancestatus: 'Attending', profilePicture:'./img/avatars/1.jpg'},
                    {id:1, name:'Gordon Lightfoot', attendancestatus: 'Awaiting Response', profilePicture:'./img/avatars/3.jpg'},
                    {id:2, name:'Mark Pearson', attendancestatus: 'Not Attending', profilePicture:'./img/avatars/4.jpg'},
                    {id:3, name:'Monica Leung', attendancestatus: 'Attending', profilePicture:'./img/avatars/2.jpg'},
                    {id:4, name:'Toby Ray', attendancestatus: 'Awaiting Response', profilePicture:'./img/avatars/7.jpg'},
                    {id:5, name:'Phoebe Nelson', attendancestatus: 'Attending', profilePicture:'./img/avatars/6.jpg'},
                    {id:6, name:'Mike Carr', attendancestatus: 'Awaiting Response', profilePicture:'./img/avatars/5.jpg'},
                    {id:7, name:'Colin Ray', attendancestatus: 'Attending', profilePicture:'./img/avatars/8.jpg'}
                ]},
            { id: 3, name: 'Coffee Date',  date: '14/08/2014', time: '21:30', venue:'TBA', meetImage:'./img/meets/icon-coffee.png', host:'Millie Southbrooke', accepted:'False', declined:'False', isNew:'False',
                attendees:[
                    {id:0, name:'Johnny Thomas', attendancestatus: 'Attending', profilePicture:'./img/avatars/1.jpg'},
                    {id:1, name:'Gordon Lightfoot', attendancestatus: 'Awaiting Response', profilePicture:'./img/avatars/3.jpg'},
                    {id:2, name:'Mark Pearson', attendancestatus: 'Not Attending', profilePicture:'./img/avatars/4.jpg'},
                    {id:3, name:'Monica Leung', attendancestatus: 'Attending', profilePicture:'./img/avatars/2.jpg'},
                    {id:4, name:'Toby Ray', attendancestatus: 'Awaiting Response', profilePicture:'./img/avatars/7.jpg'},
                    {id:5, name:'Phoebe Nelson', attendancestatus: 'Attending', profilePicture:'./img/avatars/6.jpg'},
                    {id:6, name:'Mike Carr', attendancestatus: 'Awaiting Response', profilePicture:'./img/avatars/5.jpg'},
                    {id:7, name:'Colin Ray', attendancestatus: 'Attending', profilePicture:'./img/avatars/8.jpg'}
                ]},
            { id: 4, name: 'BBQ in the Park',  date: '24/08/2014', time: '21:30', venue:'TBA', meetImage:'./img/meets/icon-bbq.png', host:'Kat', accepted:'False', declined:'True', isNew:'False',
                attendees:[
                    {id:0, name:'Johnny Thomas', attendancestatus: 'Attending', profilePicture:'./img/avatars/1.jpg'},
                    {id:1, name:'Gordon Lightfoot', attendancestatus: 'Awaiting Response', profilePicture:'./img/avatars/3.jpg'},
                    {id:2, name:'Mark Pearson', attendancestatus: 'Not Attending', profilePicture:'./img/avatars/4.jpg'},
                    {id:3, name:'Monica Leung', attendancestatus: 'Attending', profilePicture:'./img/avatars/2.jpg'},
                    {id:4, name:'Toby Ray', attendancestatus: 'Awaiting Response', profilePicture:'./img/avatars/7.jpg'},
                    {id:5, name:'Phoebe Nelson', attendancestatus: 'Attending', profilePicture:'./img/avatars/6.jpg'},
                    {id:6, name:'Mike Carr', attendancestatus: 'Awaiting Response', profilePicture:'./img/avatars/5.jpg'},
                    {id:7, name:'Colin Ray', attendancestatus: 'Attending', profilePicture:'./img/avatars/8.jpg'}
                ]},
            { id: 5, name: 'Clubbing for Really, Really Good Looking People',  date: '01/09/2014', time: '21:30', venue:'Home Bar, Darling Harbour', meetImage:'./img/meets/icon-alcohol.png', host:'John Doe', accepted:'False', declined:'False', isNew:'No',
                attendees:[
                    {id:0, name:'Johnny Thomas', attendancestatus: 'Attending', profilePicture:'./img/avatars/1.jpg'},
                    {id:1, name:'Gordon Lightfoot', attendancestatus: 'Awaiting Response', profilePicture:'./img/avatars/3.jpg'},
                    {id:2, name:'Mark Pearson', attendancestatus: 'Not Attending', profilePicture:'./img/avatars/4.jpg'},
                    {id:3, name:'Monica Leung', attendancestatus: 'Attending', profilePicture:'./img/avatars/2.jpg'},
                    {id:4, name:'Toby Ray', attendancestatus: 'Awaiting Response', profilePicture:'./img/avatars/7.jpg'},
                    {id:5, name:'Phoebe Nelson', attendancestatus: 'Attending', profilePicture:'./img/avatars/6.jpg'},
                    {id:6, name:'Mike Carr', attendancestatus: 'Awaiting Response', profilePicture:'./img/avatars/5.jpg'},
                    {id:7, name:'Colin Ray', attendancestatus: 'Attending', profilePicture:'./img/avatars/8.jpg'}
                ]},
            { id: 6, name: 'Meet for Coffees',  date: '14/07/2014', time: '21:30', venue:'TBA', meetImage:'./img/meets/icon-coffee.png', host:'John Doe', accepted:'False',declined:'False', isNew:'True',
                attendees:[
                    {id:0, name:'Johnny Thomas', attendancestatus: 'Attending', profilePicture:'./img/avatars/1.jpg'},
                    {id:1, name:'Gordon Lightfoot', attendancestatus: 'Awaiting Response', profilePicture:'./img/avatars/3.jpg'},
                    {id:2, name:'Mark Pearson', attendancestatus: 'Not Attending', profilePicture:'./img/avatars/4.jpg'},
                    {id:3, name:'Monica Leung', attendancestatus: 'Attending', profilePicture:'./img/avatars/2.jpg'},
                    {id:4, name:'Toby Ray', attendancestatus: 'Awaiting Response', profilePicture:'./img/avatars/7.jpg'},
                    {id:5, name:'Phoebe Nelson', attendancestatus: 'Attending', profilePicture:'./img/avatars/6.jpg'},
                    {id:6, name:'Mike Carr', attendancestatus: 'Awaiting Response', profilePicture:'./img/avatars/5.jpg'},
                    {id:7, name:'Colin Ray', attendancestatus: 'Attending', profilePicture:'./img/avatars/8.jpg'}
                ]},
            { id: 7, name: 'After work BBQ',  date: '24/07/2014', time: '21:30', venue:'TBA', meetImage:'./img/meets/icon-bbq.png', host:'John Doe', accepted:'False',declined:'True', isNew:'False',
                attendees:[
                    {id:0, name:'Johnny Thomas', attendancestatus: 'Attending', profilePicture:'./img/avatars/1.jpg'},
                    {id:1, name:'Gordon Lightfoot', attendancestatus: 'Awaiting Response', profilePicture:'./img/avatars/3.jpg'},
                    {id:2, name:'Mark Pearson', attendancestatus: 'Not Attending', profilePicture:'./img/avatars/4.jpg'},
                    {id:3, name:'Monica Leung', attendancestatus: 'Attending', profilePicture:'./img/avatars/2.jpg'},
                    {id:4, name:'Toby Ray', attendancestatus: 'Awaiting Response', profilePicture:'./img/avatars/7.jpg'},
                    {id:5, name:'Phoebe Nelson', attendancestatus: 'Attending', profilePicture:'./img/avatars/6.jpg'},
                    {id:6, name:'Mike Carr', attendancestatus: 'Awaiting Response', profilePicture:'./img/avatars/5.jpg'},
                    {id:7, name:'Colin Ray', attendancestatus: 'Attending', profilePicture:'./img/avatars/8.jpg'}
                ]},
            { id: 8, name: 'Engagement Drinks',  date: '22/09/2014', time: '21:30', venue:'Home Bar, Darling Harbour', meetImage:'./img/meets/icon-alcohol.png', host:'John Doe', accepted:'False',declined:'False', isNew:'True',
                attendees:[
                    {id:0, name:'Johnny Thomas', attendancestatus: 'Attending', profilePicture:'./img/avatars/1.jpg'},
                    {id:1, name:'Gordon Lightfoot', attendancestatus: 'Awaiting Response', profilePicture:'./img/avatars/3.jpg'},
                    {id:2, name:'Mark Pearson', attendancestatus: 'Not Attending', profilePicture:'./img/avatars/4.jpg'},
                    {id:3, name:'Monica Leung', attendancestatus: 'Attending', profilePicture:'./img/avatars/2.jpg'},
                    {id:4, name:'Toby Ray', attendancestatus: 'Awaiting Response', profilePicture:'./img/avatars/7.jpg'},
                    {id:5, name:'Phoebe Nelson', attendancestatus: 'Attending', profilePicture:'./img/avatars/6.jpg'},
                    {id:6, name:'Mike Carr', attendancestatus: 'Awaiting Response', profilePicture:'./img/avatars/5.jpg'},
                    {id:7, name:'Colin Ray', attendancestatus: 'Attending', profilePicture:'./img/avatars/8.jpg'}
                ]}
        ];

        return {
            all: function() {
                return meets;
            },
            get: function(meetId) {
                // Simple index lookup
                return meets[meetId];
            }
        }
    });