angular.module('simpleSocial.suggestionsController', [])

    .controller('SuggestionsCtrl', function($scope, SuggestionService, MeetService) {
        $scope.suggestions = SuggestionService.all();
        console.log(SuggestionService);
        $scope.alert = function(text) {
            alert(text);
        };

        $scope.welcomeMessage = 'Hi, welcome to the Angular tutorial';

        $scope.meets = MeetService.all();
        $scope.currentDate = new Date();

    })




    .controller('SuggestionCtrl', function($scope, $stateParams, SuggestionService){
        $scope.suggestion = SuggestionService.get($stateParams.suggestionId);
        $scope.alert = function(text) {
            alert(text);
        };
    })



.controller('Profile2Ctrl', function($scope) {
    $scope.profileOptions = [
        { optionName: 'My Name', currentValue: 'Jessica Tesla', id: 1  },
        { optionName: 'E-mail', currentValue: 'JessTesla@gmail.com', id: 2 },
        { optionName: 'Phone', currentValue: '0400000000', id: 3 },
        { optionName: 'Password', currentValue: '*******', id: 4 },
        { optionName: 'Location', currentValue: 'Sydney', id: 5 },
        { optionName: 'GPS-coordinates', currentValue: '-33.867383, 151.207026', id: 6 }
    ];
});