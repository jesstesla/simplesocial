angular.module('simpleSocial.contactServices', [])

/**
 * A simple example service that returns some data.
 */
    .factory('ContactService', function() {
        // Might use a resource here that returns a JSON array

        // Some fake testing data
        var contacts = [
            { id: 0, ssNick:'DouglasJ', name: 'John Douglas', profilePicture:'./img/avatars/1.jpg',  address: '1 George street', phone:'0400000000', currentGPS:'33.868630, 151.207122', currentCity:'Sydney, NSW Australia', daysSinceLastMeet:'365',

                socialAccounts:[
                    {id:1, name:'Facebook', icon:'ion-social-facebook'},
                    {id:2, name:'Google', icon:'ion-social-googleplus'},
                    {id:3, name:'Mobile', icon:'ion-iphone'},
                    {id:4, name:'E-Mail', icon:'ion-email'}
                ],
                foodPreferences:[
                    {id:1, name:'Ovo-lacto Vegetarian'}
                ],
                considerations: [
                    {id:1, name: 'Child friendly'}

                ]

            },
            { id: 1, ssNick:'KellyDeal', name: 'Kelly Deal', profilePicture:'./img/avatars/2.jpg', address: '1 George street', phone:'0400000000', currentGPS:'33.868630, 151.207122', currentCity:'Sydney, NSW Australia', daysSinceLastMeet:'365',
                 socialAccounts:[
                     {id:1, name:'Facebook', icon:'ion-social-facebook'},
                     {id:2, name:'Google', icon:'ion-social-googleplus'},
                     {id:3, name:'E-Mail', icon:'ion-email'}
                  ],
                foodPreferences:[
                    {id:1, name:'Gluten Intolerant'},
                    {id:2, name:'Diabetic'},
                    {id:3, name:'Vegan'},
                    {id:4, name:'No Spicy'}
                ],
                considerations: [
                    {id:1, name: 'Child friendly'},
                    {id:2, name: 'Requires wheelchair'}
                ]
            },

            { id: 2, ssNick:'FreddyM', name: 'Fred Mercury', profilePicture:'./img/avatars/3.jpg', address: '1 George street', phone:'0400000000', currentGPS:'-33.868630, 151.207122', currentCity:'Melbourne, VIC Australia',  daysSinceLastMeet:'90',
                 socialAccounts:[
                     {id:1, name:'Google', icon:'ion-social-googleplus'},
                     {id:2, name:'Mobile', icon:'ion-iphone'},
                     {id:3, name:'E-Mail', icon:'ion-email'}
                   ],
                foodPreferences:[
                    {id:1, name:'See-food: Eats everything they see'}

                ],
                considerations: [
                    {id:1, name:'None'}
                ]
            },

            { id: 3, ssNick:'IndieJ', name: 'Indie Jones', profilePicture:'./img/avatars/4.jpg', address: '1 George street', phone:'0400000000', currentGPS:'-33.868630, 151.207122', currentCity:'Brisbane, QLD Australia', daysSinceLastMeet:'28',
                socialAccounts:[
                    {id:1, name:'Facebook', icon:'ion-social-facebook'},
                    {id:2, name:'Mobile', icon:'ion-iphone'},
                    {id:3, name:'E-Mail', icon:'ion-email'}
                  ],
                foodPreferences:[
                    {id:1, name:'Halal'}

                ],
                considerations: [
                    {id:1, name: 'Child friendly'}

                ]
            },

            { id: 4,  ssNick:'RadalMcgee', name: 'Randal McGee', profilePicture:'./img/avatars/5.jpg', address: '1 George street', phone:'0400000000', currentGPS:'-33.868630, 151.207122', currentCity:'North Ryde, NSW Australia', daysSinceLastMeet:'10',
                socialAccounts:[
                    {id:1, name:'Mobile', icon:'ion-iphone'},
                    {id:2, name:'E-Mail', icon:'ion-email'}
                  ],
                foodPreferences:[
                    {id:1, name:'No Spicy'}
                ],
                considerations: [
                    {id:1, name: 'Child friendly'},
                    {id:2, name: 'Requires wheelchair'}
                ]
            },

            { id: 5,  ssNick:'TehCoops', name: 'Carla Neeson', profilePicture:'./img/avatars/6.jpg', address: '1 George street', phone:'0400000000',currentGPS:'-33.868630, 151.207122', currentCity:'Pyrmont, NSW Australia', daysSinceLastMeet:'7',

                socialAccounts:[
                    {id:1, name:'Facebook', icon:'ion-social-facebook'},
                    {id:2, name:'Google', icon:'ion-social-googleplus'},
                    {id:3, name:'Mobile', icon:'ion-iphone'},
                    {id:4, name:'E-Mail', icon:'ion-email'}
                  ],
                foodPreferences:[
                    {id:1, name:'Ovo-lacto Vegetarian'}

                ],
                considerations: [
                    {id:1, name:'None'}
                ]
            }
            ];
        return {
            all: function() {
                return contacts;
            },
            get: function(contactId) {
                // Simple index lookup
                return contacts[contactId];
            }
        }
    });