angular.module('simpleSocial.profileController', [])

    .controller('ProfileCtrl', function($scope) {
    $scope.profileOptions = [
            { optionName: 'My Name', currentValue: 'Jessica Tesla', id: 1  },
            { optionName: 'E-mail', currentValue: 'JessTesla@gmail.com', id: 2 },
            { optionName: 'Phone', currentValue: '0400000000', id: 3 },
            { optionName: 'Password', currentValue: '*******', id: 4 },
            { optionName: 'Location', currentValue: 'Sydney', id: 5 },
            { optionName: 'GPS-coordinates', currentValue: '-33.867383, 151.207026', id: 6 }
        ];
});




