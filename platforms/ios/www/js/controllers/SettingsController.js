angular.module('simpleSocial.settingsController', [])

.controller('SettingsCtrl', function($scope) {
    $scope.settings = [
        { id: 1, name: 'John Wang',  address: '1 George street', phone:'0400000000',
            socialAccounts:[
                {id:1, name:'Facebook', icon:'ion-social-facebook'},
                {id:2, name:'Google', icon:'ion-social-googleplus'},
                {id:3, name:'Mobile', icon:'ion-iphone'},
                {id:4, name:'E-Mail', icon:'ion-email'}
            ] },
        { id: 2, name: 'Kelly Deal', address: '1 George street', phone:'0400000000',
            socialAccounts:[
                {id:1, name:'Facebook', icon:'ion-social-facebook'},
                {id:2, name:'Google', icon:'ion-social-googleplus'},
                {id:3, name:'E-Mail', icon:'ion-email'}
            ] },

        { id: 3, name: 'Fred Mercury', address: '1 George street', phone:'0400000000',
            socialAccounts:[
                {id:1, name:'Google', icon:'ion-social-googleplus'},
                {id:2, name:'Mobile', icon:'ion-iphone'},
                {id:3, name:'E-Mail', icon:'ion-email'}
            ] },

        { id: 4, name: 'Indie Jones', address: '1 George street', phone:'0400000000',
            socialAccounts:[
                {id:1, name:'Facebook', icon:'ion-social-facebook'},
                {id:3, name:'Mobile', icon:'ion-iphone'},
                {id:4, name:'E-Mail', icon:'ion-email'}
            ] },

        { id: 5, name: 'Rada McGee', address: '1 George street', phone:'0400000000',
            socialAccounts:[
                {id:1, name:'Mobile', icon:'ion-iphone'},
                {id:2, name:'E-Mail', icon:'ion-email'}
            ]  },

        { id: 6,name: 'Cooper Neeson', address: '1 George street', phone:'0400000000',
            socialAccounts:[
                {id:1, name:'Facebook', icon:'ion-social-facebook'},
                {id:2, name:'Google', icon:'ion-social-googleplus'},
                {id:3, name:'Mobile', icon:'ion-iphone'},
                {id:4, name:'E-Mail', icon:'ion-email'}
            ] }
    ];
});

