angular.module('simpleSocial.meetServices', [])

/**
 * A simple example service that returns some data.
 */
    .factory('MeetService', function() {
        // Might use a resource here that returns a JSON array

        // Some fake testing data
        var meets = [
            { id: 1, name: 'John\'s Birthday',  date: '01/07/2014', time: '21:30', venue:'Fifty-Fifty Bar, Darlinghurst' },
            { id: 2, name: 'Graduation Party',  date: '20/07/2014', time: '20:30', venue:'Lord Nelson, The Rocks' },
            { id: 3, name: 'Random Dinner party',  date: '22/08/2014', time: '17:30', venue:'Bar Reggio, Darlinghurst' },
            { id: 4, name: 'Coffee Date',  date: '14/08/2014', time: '21:30', venue:'TBA' },
            { id: 5, name: 'BBQ in the Park',  date: '24/08/2014', time: '21:30', venue:'TBA' },
            { id: 6, name: 'Clubbing for Really, Really Good Looking People',  date: '01/09/2014', time: '21:30', venue:'Home Bar, Darling Harbour' }
        ];

        return {
            all: function() {
                return meets;
            },
            get: function(meetId) {
                // Simple index lookup
                return meets[meetId];
            }
        }
    });