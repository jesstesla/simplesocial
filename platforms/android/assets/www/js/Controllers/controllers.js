angular.module('simpleSocial.controllers', [])

.controller('AppCtrl', function($scope) {
})

.controller('PlaylistsCtrl', function($scope) {
  $scope.playlists = [
    { title: 'Reggae', id: 1 },
    { title: 'Chill', id: 2 },
    { title: 'Dubstep', id: 3 },
    { title: 'Indie', id: 4 },
    { title: 'Rap', id: 5 },
    { title: 'Cowbell', id: 6 }
  ];
})



.controller('PlaylistCtrl', function($scope, $stateParams) {
})



    .controller('ContactsCtrl', function($scope, ContactService) {
        $scope.contacts = ContactService.all();
        $scope.alert = function(text) {
            alert(text);
        };
    })


    .controller('ContactCtrl', function($scope, $stateParams, ContactService){
        $scope.contact = ContactService.get($stateParams.contactId);
        $scope.alert = function(text) {
            alert(text);
        };
    });



/***
.controller('ContactCtrl', function($scope, $stateParams) {

            $scope.selectContact = function(contact, index) {
            $scope.activeContact = contact;
            $scope.contacts.setLastActiveIndex(index);
            //$scope.sideMenuController.close();
        };


        // Called to select the given project
        $scope.selectContact = function(contact, index) {
            $scope.activeContact = contact;
            Contacts.setLastActiveIndex(index);
            //$scope.sideMenuController.close();
        };
    }) ;

     ***/