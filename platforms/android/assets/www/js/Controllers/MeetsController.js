angular.module('simpleSocial.meetsController', [])

    .controller('MeetsCtrl', function($scope, MeetService) {
        $scope.meets = MeetService.all();
        $scope.alert = function(text) {
            alert(text);
        };
    })


    .controller('MeetCtrl', function($scope, $stateParams, MeetService){
        $scope.meet = MeetService.get($stateParams.meetId);
        $scope.alert = function(text) {
            alert(text);
        };
    });
